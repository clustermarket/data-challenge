# Data challenge

We recommend Python and Pandas, but any tool of your choice will do.

## Dataset

Download the [`avocados.csv`][avocados] file from [here][avocados]. Inside you will find a data set of avocado information. The target variable is contained in the `Price` column while the other columns represent avocado features.

## Task

1. Fit a standard multilinear regression model which uses all the predictors to understand which features correlate with the price of avocados and which don't.
2. Create a model that will predict avocado prices based on the specified features.
3. Evaluate the accuracy of the model.

[avocados]: https://gitlab.com/clustermarket/data-challenge/-/raw/main/avocados.csv?inline=false
